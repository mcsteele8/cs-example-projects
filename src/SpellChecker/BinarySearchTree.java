
package SpellChecker;



import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree<E extends Comparable<E>> {
    private TreeNode<E> root;



    public boolean search(E value) { //searches the BST to find  the value you are looking for
        TreeNode<E> current = root;// setting current to the start of the BST
        boolean found = false;
        while (current != null && !found) { // if you have not found the value keep on searching
            if (current.value.equals(value)) {
                found = true;
            }
            else if (current.value.compareTo(value) < 0) {
                current = current.right;
            }
            else {
                current = current.left;
            }
        }
        return found;
    }
    public boolean insert(E value) {
        TreeNode<E> node = new TreeNode<>(value);
        if (root == null) {
            root = node;
            return true;
        }
        else {
            TreeNode<E> parent = null;
            TreeNode<E> current = root;
            while (current != null) {
                parent = current;
                if(current.value.compareTo(value)==0){
                    return false;
                }
                else if (current.value.compareTo(value) < 0) {

                    current = current.right;
                }
                else {

                    current = current.left;
                }
            }
            if (parent.value.compareTo(value) < 0) {
                parent.right = node;
                return true;
            }
            else {
                parent.left = node;
                return true;
            }
        }

    }
    public boolean remove(E value) {
        TreeNode<E> parent = null;
        TreeNode<E> node = root;
        boolean done = false;



        while (!done) {
            if(node == null){
                return true;
            }

            if (node.value.compareTo(value) < 0) {
                parent = node;
                node = node.right;
            }
            else if (node.value.compareTo(value) > 0) {
                parent = node;
                node = node.left;
            }

            else {
                done = true;
            }
        }// No left child case
        //
        if (node.left == null) {
            if (parent == null) {
                root = node.right;
                return true;
            }
            else {
                if (parent.value.compareTo(value) < 0) {
                    parent.right = node.right;
                }
                else {
                    parent.left = node.right;
                }
            }
            return true;
        }
        else {
            TreeNode<E> parentOfRight = node;
            TreeNode<E> rightMost = node.left;
            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }
            node.value = rightMost.value;
            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
            }
            else {
                parentOfRight.left = rightMost.left;
            }
            return true;
        }
        //return false;

    }



    public void display(String message) {
        System.out.println("");
        System.out.println(message);
        System.out.println(" ");
        traverseInOrder(root);
        System.out.println(" ");
    }

    private void traverseInOrder(TreeNode<E> node) {
        if (node != null) {
            traverseInOrder(node.left);
            System.out.println(node.value);
            traverseInOrder(node.right);
        }
    }

    public int numberNodes(){
        if(root == null){
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();

        queue.add(root);

        int count = 1;
        int counts = 0;
        while (!queue.isEmpty()){ //
            counts++;
            TreeNode temp = queue.poll();
            if(temp.left != null && temp.right != null){

                count++;
            }
            if(temp.left!= null){
                queue.add(temp.left);
            }
            if(temp.right != null){
                queue.add(temp.right);
            }
            if(temp.right == null && temp.left == null){
                count++;
            }

        }

        return counts;// += numberLeafNodes();
    }



    public int numberLeafNodes(){
        return numberLeafNodes(root);
    }

    private int numberLeafNodes(TreeNode<E> node){
        if (node == null){
            return 0;
        }
        if (node.left == null && node.right == null){
            return 1;
        }
        else return numberLeafNodes(node.left)+ numberLeafNodes(node.right);


    }

    public int height(){
        return height(root)-1;
    }

    private int height(TreeNode node){
        if(node == null){
            return 0;
        }
        else {
            int lDepth = height(node.left);
            int rDepth = height(node.right);

            if(lDepth > rDepth){
                return (lDepth +1);
            }
            else {
                return (rDepth +1);
            }
        }
    }

    private class TreeNode<E> {
        public E value;
        public TreeNode<E> left;
        public TreeNode<E> right;
        public TreeNode(E value) {
            this.value = value;
        }
    }






}


