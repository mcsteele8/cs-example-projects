
package SpellChecker;

import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

public class SpellChecker {
    public static void main(String[] args) {



        BinarySearchTree<String> dictionary = readDictionary();
        reportTreeStats(dictionary);

        testTree();


    }



    public static void testTree() {
        BinarySearchTree<String> tree = new BinarySearchTree<>();

        //
        // Add a bunch of values to the tree
        tree.insert("Olga");
        tree.insert("Tomeka");
        tree.insert("Benjamin");
        tree.insert("Ulysses");
        tree.insert("Tanesha");
        tree.insert("Judie");
        tree.insert("Tisa");
        tree.insert("Santiago");
        tree.insert("Chia");
        tree.insert("Arden");

        //
        // Make sure it displays in sorted order
        tree.display("--- Initial Tree State ---");
        reportTreeStats(tree);

        //
        // Try to add a duplicate
        if (tree.insert("Tomeka")) {
            System.out.println("oops, shouldn't have returned true from the insert");
        }
        tree.display("--- After Adding Duplicate ---");
        reportTreeStats(tree);

        //
        // Remove some existing values from the tree
        tree.remove("Olga");    // Root node
        tree.remove("Arden");   // a leaf node
        tree.display("--- Removing Existing Values ---");
        reportTreeStats(tree);

        //
        // Remove a value that was never in the tree, hope it doesn't crash!
        tree.remove("Karl");
        tree.display("--- Removing A Non-Existent Value ---");
        reportTreeStats(tree);
    }

    public static void reportTreeStats(BinarySearchTree<String> tree) {
        System.out.println("-- Tree Stats --");
        System.out.printf("Total Nodes : %d\n", tree.numberNodes());
        System.out.printf("Leaf Nodes  : %d\n", tree.numberLeafNodes());
        System.out.printf("Tree Height : %d\n", tree.height());
    }

    public static BinarySearchTree<String> readDictionary() {


        BinarySearchTree<String> dictionary = new BinarySearchTree<>();
        ArrayList<String> readInDictionary = new ArrayList<>();
        ArrayList<String> readInLetter = new ArrayList<>();

        try (Scanner input = new Scanner(new File("Dictionary.txt"))) {



            while (input.hasNext()) {

                String name = input.next();
                readInDictionary.add(name);


            }
            java.util.Collections.shuffle(readInDictionary, new java.util.Random(System.currentTimeMillis()));
            for(int i = 0; i < readInDictionary.size(); i++) {
                dictionary.insert(readInDictionary.get(i));
            }

        }
        catch (Exception ex) {
            System.out.println("Something bad happened");
        }



        try (Scanner input = new Scanner(new File("Letter.txt"))) {     // If you are going to be checking this against different letters, insert file name here.



            while (input.hasNext()) {
                String name = input.next();
                readInLetter.add(name.replaceAll("[^a-zA-Z ]", "").toLowerCase());
                //letter.insert(name.toLowerCase());



            }


        }
        catch (Exception ex) {
            System.out.println("Something bad happened");
        }

        System.out.println("You have misspelled these words: ");
        for(int i = 0; i < readInLetter.size(); i++){
            if(!dictionary.search(readInLetter.get(i))){
                System.out.println(readInLetter.get(i));
            }
        }
        System.out.println();



        return dictionary;
    }
}